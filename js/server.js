game.Server = Object.extend({
    handlers : {},

    _onOpen : function() {
        console.log('Connection with', this.url, 'established');
    },

    _onClose : function(event) {
        if (event.wasClean) {
            console.log('Connection closed cleanly');
        } else {
            console.log('Connection unexpectedly terminated');
            console.log('Code: ' + event.code + ' Reason: ' + event.reason);
        }
    },

    _onMessage : function(event) {
        // console.log('Received from server (raw):', event.data);
        var data = JSON.parse(event.data);
        console.log('Received from server (parsed):', data);
        eventName = data.type;
        handlers = this.handlers[eventName] || []
        if (handlers.length == 0){
            console.log('No handlers registered for event', eventName);
            return
        }
        for (var i = 0; i < handlers.length; i++){
            handler = handlers[i];
            handler.apply(this, [data.body]);
        }

    },

    connect : function(url){
        this.url = url;
        this.ws = new WebSocket(url);
        this.ws.onopen = this._onOpen.bind(this);
        this.ws.onclose = this._onClose.bind(this);
        this.ws.onmessage = this._onMessage.bind(this);
    },

    registerHandler : function(eventName, handler){
        this.handlers[eventName] = this.handlers[eventName] || []
        this.handlers[eventName].push(handler)
    },

    send : function(data){
        if (this.ws.readyState === 1) {
            console.log('Sending', data)
            this.ws.send(JSON.stringify(data));
        } else {
            var self = this;
            setTimeout(function () {
                self.send(data);
            }, 100);
        }
    }

});
game.Server.EVENT_INIT = 0;
game.Server.EVENT_MOVE = 1;
game.Server.EVENT_NEW_PLAYER_JOINED = 2;
game.Server.REQ_JOIN = 3;
game.Server.EVENT_JOIN_ACCEPT = 4;
game.Server.REQ_MOVE = 5;
game.Server.EVENT_OBJ_MOVED = 6;
game.Server.EVENT_OBJ_REMOVED = 7;
