game.BulletEntity = me.ObjectEntity.extend({

    z : 1000,
    speed : 10,

    init : function(x, y, settings) {
        image = me.loader.getImage("bullet")
        settings = settings !== undefined ? settings : {};
        settings.spritewidth = image.width;
        settings.spriteheight = image.height;
        settings.width = image.width;
        settings.height = image.height;
        settings.image = "bullet";
        this.parent(x, y, settings);

        this.setVelocity(this.speed, this.speed);
        this.vel.x = 0.005;
        this.vel.y = 0.005;
        // this.vel.x = 10;
        this.counter = 0;
    },

    update : function(dt) {
        this.counter += 0.03;
        if (this.vel.x == 0 || this.vel.y == 0){
            me.game.world.removeChild(this);
        } else {
            this.vel.x = this.speed;
            // this.vel.y = this.speed / 2;
        }
        this.updateMovement();
        this.parent(dt);
        return true;
    }

});


game.PlayerEntity = me.ObjectEntity.extend({

    z : 999,
    mainPlayerEntity : false,

    init : function(x, y, settings) {
        settings = settings !== undefined ? settings : {}
        settings.image = "player"
        settings.width = 16
        settings.height = 32
        settings.spritewidth = 32
        settings.sptiteheight = 32
        this.parent(x, y, settings);
        this.setVelocity(1, 1);

    },

    shoot : function(){
        console.log('player shooted');
        var bullet = me.pool.pull("bullet", this.pos.x, this.pos.y  + (this.height/2));
        me.game.world.addChild(bullet);
    },

    move : function(direction) {
        if (direction == 'left') {
            this.vel.y = 0;
            this.vel.x -= this.accel.x * me.timer.tick;
        } else if (direction == 'right') {
            this.vel.y = 0;
            this.vel.x += this.accel.x * me.timer.tick;
        } else if (direction == 'down') {
            this.vel.x = 0;
            this.vel.y += this.accel.y * me.timer.tick;
        } else if (direction == 'up') {
            this.vel.x = 0;
            this.vel.y -= this.accel.y * me.timer.tick;
        } else {
            throw "Unknown direction '" + direction + "'";
        }
    },

    stop : function(){
        this.vel.x = 0;
        this.vel.y = 0;
    },

    draw : function(context) {
        this.parent(context);

        testText = new me.Font("Verdana", 12, "white");
        testText.draw(context, this.serverName, this.pos.x - 14, this.pos.y);
    }

});


game.RemotePlayerEntity = game.PlayerEntity.extend({


});

/*-------------------
a player entity
-------------------------------- */
game.MainPlayerEntity = game.PlayerEntity.extend({

    mainPlayerEntity : true,

    init: function(x, y, settings) {
        this.parent(x, y, settings);
        // set the display to follow our position on both axis
        me.game.viewport.follow(this.pos, me.game.viewport.AXIS.BOTH);
    },

    update : function(dt) {
        if (me.input.isKeyPressed('left')) {
            this.move('left');
        } else if (me.input.isKeyPressed('right')) {
            this.move('right');
        } else if (me.input.isKeyPressed('down')) {
            this.move('down');
        } else if (me.input.isKeyPressed('up')) {
            this.move('up');
        } else {
            this.stop();
        }

        // if (me.input.isKeyPressed('shoot')){
        //     this.shoot();
        // }

        // check & update player movement
        this.updateMovement();

        // update animation if necessary
        if (this.vel.x!=0 || this.vel.y!=0) {
            // update object animation
            this.parent(dt);
            return true;
        }

        // else inform the engine we did not perform
        // any update (e.g. position, animation)
        return false;
    },


    prevSentPos: {
        x: null,
        y: null
    },

    updateMovement: function() {
        res = this.parent();
        if (this.prevSentPos.x != this.pos.x || this.prevSentPos.y != this.pos.y){
            game.server.send({
                'type': game.Server.REQ_MOVE,
                'body': {
                    'name': this.serverName,
                    'pos':{
                        'x': this.pos.x,
                        'y': this.pos.y,
                    }
                }
            });
            this.prevSentPos.x = this.pos.x;
            this.prevSentPos.y = this.pos.y;
        }
        return res;
    }

});
