game.resources = [

	/* Graphics.
	 * @example
	 * {name: "example", type:"image", src: "data/img/example.png"},
	 */
	 {name: "foresttiles_0", type:"image", src: "data/img/map/foresttiles_0.gif"},
	 {name: "metatiles16x16", type:"image", src: "data/img/map/metatiles16x16.png"},
	 {name: "player", type:"image", src: "data/img/sprite/player.png"},
	 {name: "bullet", type:"image", src: "data/img/sprite/bullet.png"},

	/* Atlases
	 * @example
	 * {name: "example_tps", type: "tps", src: "data/img/example_tps.json"},
	 */

	/* Maps.
	 * @example
	 * {name: "example01", type: "tmx", src: "data/map/example01.tmx"},
	 * {name: "example01", type: "tmx", src: "data/map/example01.json"},
 	 */

 	 {name: "arena01", type: "tmx", src: "data/map/arena01.tmx"}

	/* Background music.
	 * @example
	 * {name: "example_bgm", type: "audio", src: "data/bgm/"},
	 */

	/* Sound effects.
	 * @example
	 * {name: "example_sfx", type: "audio", src: "data/sfx/"}
	 */
];
