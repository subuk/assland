game.PlayScreen = me.ScreenObject.extend({

	objects: {},

	addMainPlayer : function(objInfo) {
		console.log('Creating main player', objInfo);
		var obj = new game.MainPlayerEntity(objInfo.pos.x, objInfo.pos.y);
		obj.serverName = objInfo.name;
		this.objects[objInfo.name] = obj;
		me.game.world.addChild(obj);
	},

	addRemotePlayer : function(objInfo) {
		console.log('Creating remote player', objInfo)
		var obj = new game.RemotePlayerEntity(objInfo.pos.x, objInfo.pos.y);
		obj.serverName = objInfo.name;
		this.objects[objInfo.name] = obj;
		me.game.world.addChild(obj);
	},

	addBullet : function(objInfo) {
		console.log('Creating bullet', objInfo)
		var obj = new game.BulletEntity(objInfo.pos.x, objInfo.pos.y);
		obj.serverName = objInfo.name;
		me.game.world.addChild(obj);
	},

	/**
	 *  action to perform on state change
	 */
	onResetEvent : function() {

		me.levelDirector.loadLevel("arena01");

		// reset the score
		game.data.score = 0;

		game.server = new game.Server()

		self = this
		game.server.registerHandler(game.Server.EVENT_INIT, function(data){
			_(data.objects).each(function(obj){
				if (obj.type == "Player"){
					self.addRemotePlayer(obj);
				} else if (obj.type == "Bullet"){
					self.addBullet(obj);
				}
			})
		});

		game.server.registerHandler(game.Server.EVENT_JOIN_ACCEPT, function(objInfo){
			console.log("Join accept received");
			self.addMainPlayer(objInfo);
		})

		game.server.registerHandler(game.Server.EVENT_NEW_PLAYER_JOINED, function(objInfo){
			self.addRemotePlayer(objInfo);
		});

		game.server.registerHandler(game.Server.EVENT_OBJ_MOVED, function(objInfo){
			console.log("Object moved event received", objInfo)
			obj = self.objects[objInfo.name];
			if (!obj.mainPlayerEntity){
				obj.pos.x = objInfo.pos.x;
				obj.pos.y = objInfo.pos.y;
			}
		});

		game.server.registerHandler(game.Server.EVENT_OBJ_REMOVED, function(objInfo){
			console.log("Object remove event reveived", objInfo);
			obj = self.objects[objInfo.name];
			me.game.world.removeChild(obj);
		})

		game.server.connect('ws://' + window.location.hostname + '/stream')
		console.log("Join request sent, waiting accept")
		game.server.send({
			'type': game.Server.REQ_JOIN,
			'body': {
				'name': 'Me',
			}
		})

		// add our HUD to the game world
		this.HUD = new game.HUD.Container();
		me.game.world.addChild(this.HUD);
	},


	/**
	 *  action to perform when leaving this screen (state change)
	 */
	onDestroyEvent : function() {
		// remove the HUD from the game world
		me.game.world.removeChild(this.HUD);
		me.game.world.removeChild(this.player);
		game.server = null;
	}
});
